import React, { Component } from 'react';
import './App.css';
import LenarComponent from './LenarComponent';

class App extends Component {

  render() {
    return (
      <div className="App">
        <LenarComponent />
      </div>
    );
  }
}

export default App;
