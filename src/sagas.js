import { delay } from 'redux-saga'
import { put, takeEvery, all, call, fork, join, cancel, select } from 'redux-saga/effects'
import axios from 'axios';

function* addCountSaga() {
    let data = yield call(timeoutPromise, 2000);
    console.log(data);
    yield put({type: 'ADD_COUNT'})
}

function* addCountSaga2() {
    let data = yield call(timeoutPromise, 3000);
    console.log(data);
    yield put({type: 'ADD_COUNT'})
}

function* addAll() {
    let m = yield select();
    console.log(m);
    const [a, b] = yield all([
        call(timeoutPromise, 2000),
        call(timeoutPromise, 2500),
    ]);
    console.log('a');
    yield call(delay, 1000)
    console.log('all fin');
}

function* addCountAsyncSaga() {
    yield takeEvery('ADD_COUNT_ASYNC_SAGA', addAll);
}

const timeoutPromise = (timeout) => new Promise(resolve => {
    setTimeout(() => resolve('fin ' + timeout), timeout);
});

export default function* rootSaga() {
    yield all([
        addCountAsyncSaga()
    ])
}