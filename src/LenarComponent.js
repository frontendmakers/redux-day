import React, { Component } from 'react';
import {connect} from 'react-redux';
import { addCount, addCountAsync, addCountAsyncWithThunk, returnPromise } from "./actions/counts";

class LenarComponent extends Component {
    constructor() {
        super();
        this.state = {
            hello: ''
        };
        this.showHello = this.showHello.bind(this);
    }
    showHello() {
        this.setState({hello: 'hello'});
    }
    render() {
        return (
            <div>
                <h2>Lenar</h2>
                { this.props.isLoading && <div>IS LOADING</div> }
                <p>
                    {this.props.count}
                </p>
                <p> { this.state.hello }</p>
                <p>
                    <button onClick={this.props.addCount}>Add count</button>
                    <button onClick={this.props.addCountAsync}>Add count async</button>
                    <button onClick={this.props.addCountAsyncWithThunk}>Add count async with thunk</button>
                    <button onClick={this.props.addCountAsyncWithSaga}>Add count async with saga</button>
                    <button onClick={() => this.props.returnPromise().then(this.showHello)}>Promise</button>
                </p>
            </div>
        );
    }
}

const mapStateToProps = state => {
    let {count, isLoading} = state.counts;
    return {count, isLoading};
    // OR return state.counts;
};

const mapDispatchToProps = (dispatch) => ({
    addCount: () => dispatch(addCount()),
    addCountAsync: () => addCountAsync(5),
    addCountAsyncWithThunk: () => dispatch(addCountAsyncWithThunk(10)),
    addCountAsyncWithSaga: () => dispatch({type: 'ADD_COUNT_ASYNC_SAGA'}),
    returnPromise: () => dispatch(returnPromise())
});
// // OR
// const mapDispatchToProps = {
//     addCount,
//     addCountAsyncWithThunk
// };

export default connect(mapStateToProps, mapDispatchToProps)(LenarComponent);
