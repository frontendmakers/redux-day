import store from '../store';

export const addCount = (value) => ({
    type: 'ADD_COUNT',
    payload: value
});
export const setCountPending = (value) => ({
    type: 'SET_COUNT_PENDING',
    payload: value
});
export const setCountFulfilled = (value) => ({
    type: 'SET_COUNT_FULFILLED',
    payload: value
});

export const addCountAsync = (value) => {
    store.dispatch(addCount(value));
    store.dispatch(setCountPending(value));
    setTimeout(() => {
        store.dispatch(addCount(value));
        store.dispatch(setCountFulfilled(value));
    }, 3000);
};

export const addCountAsyncWithThunk = (value) => (dispatch, getState) => {
    // console.log(getState);
    dispatch(addCount(value));
    dispatch(setCountPending(value));
    setTimeout(() => {
        dispatch(addCount(value));
        dispatch(setCountFulfilled(value));
    }, 3000);
};

export const returnPromise = () => (dispatch) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(), 3000);
    });
};