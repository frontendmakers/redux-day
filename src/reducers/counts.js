const initialState = {
    count: 0,
    isLoading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case 'ADD_COUNT': {
            return {...state, count: state.count + (action.payload || 1)}
        }
        case 'ADD_COUNT_ASYNC_SAGA': {
            console.log('redabc');
            return {...state, count: state.count + 8}
        }
        case 'SET_COUNT_PENDING': {
            return {...state, isLoading: true}
        }
        case 'SET_COUNT_FULFILLED': {
            return {...state, isLoading: false}
        }
        default: {
            return state
        }
    }
}