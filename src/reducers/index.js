import counts from './counts';
import {combineReducers} from "redux";

export default combineReducers({ counts });